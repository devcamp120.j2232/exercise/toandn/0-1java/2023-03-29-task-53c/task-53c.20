import models.*;

public class App {
    public static void main(String[] args) throws Exception {
        MovablePoint point = new MovablePoint(5,6,2,3);
        MovableCircle circle = new MovableCircle(5, point);

        System.out.println(point.toString());
        System.out.println(circle.toString());

        point.moveUp();
        point.moveUp();
        point.moveLeft();

        System.out.println(point.toString());

        circle.moveLeft();
        circle.moveUp();
        circle.moveRight();

        System.out.println(circle.toString());
    }
}
